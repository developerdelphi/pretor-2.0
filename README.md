## About System

Sistema desenvolvido para gerenciamento de processos de escritório de advocacia, utilizando Laravel em sua versão mais atualizada (8.48), até o momento, podendo ser alterada até finalização do projeto.
Por conseguite, faz uso do VueJS 3 para front, tratando-se portanto de um monolito que é gerenciado pelo inertia.
A base de dados é em MYSQL em sua última versão

A compilação é realizada pelo Sail - Docker
