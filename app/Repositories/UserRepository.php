<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
  public function all()
  {
    return User::paginate(5);
  }

  public function getById($id)
  {
    return User::find($id);
  }

  public function update(User $user)
  {
    if (request()->get('password') !== null) {
      $user->password = Hash::make(request()->password);
    }

    $user->name = request()->name;
    $user->email = request()->email;

    $user->update();
  }


  public function store()
  {
    return User::create([
      'name' => request()->name,
      'email' => request()->email,
      'password' => Hash::make(request()->password),
    ]);
  }
}
