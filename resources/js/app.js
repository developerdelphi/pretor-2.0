require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';

// Imports FontAwesome
import { library, dom  } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import icons from './font-awesome-icons'
library.add(icons);
dom.watch();
// ----------------------------------------------------------------

const el = document.getElementById('app');

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, app, props, plugin }) {
        createApp({ render: () => h(app, props) })
            .mixin({ methods: { route } })
            .component('fa-icon', FontAwesomeIcon)
            .use(plugin)
            .mount(el);

    },
});

InertiaProgress.init({ color: '#4B5563' });
